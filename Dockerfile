ARG BASE_IMAGE=registry.git.rwth-aachen.de/jupyter/profiles/rwth-courses:latest
FROM ${BASE_IMAGE}

USER root

RUN apt-get update && apt-get install libgl1-mesa-glx -y

# Install packages via requirements.txt
ADD requirements.txt .
RUN pip install -r requirements.txt

# .. Or update conda base environment to match specifications in environment.yml
ADD environment.yml /tmp/environment.yml

# All packages specified in environment.yml are installed in the base environment
RUN conda env update -f /tmp/environment.yml && \
    conda clean -a -f -y

RUN source /opt/conda/etc/profile.d/conda.sh
RUN conda install ipykernel -y
RUN conda install root -y
RUN pip install jupyterlab

USER $NB_USER